// @flow
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions
} from 'react-native';
import { CloudStorage, MediaStorage, defaultLifeCycle } from 'aq-native-miniapp';
import {
  StaticCanvas
} from './components';
import Create from './misc/Create';
import Join from './views/Join';

/*
This type represents the data structure shared between create and join screens.
Basically, create screens should output this data, which will then be consumed
by the join screen
*/
export type ItemData = {
  title: string,
  coverImage: string
}

const props = {}

function Warning(props) {
  return <Text>Warning: {props.message}</Text>;
}

export default class App extends React.Component {

  static defaultProps = {
    action: 'preview'
  }

  state: {
    data: ?Object
  }

  constructor(props: Props){
    super(props);
    this.state = {
      data: {}
    }
  }

  componentWillMount(){
    defaultLifeCycle.setOnDataCallback(this._onData.bind(this));
  }

  _onData(data: ItemData) {
    this.setState({data: data});
  }

  render() {
    const action = this.props.action;
    const id = this.props.id;
    const { height, width } = Dimensions.get('window');
    let render = <StaticCanvas style={{width:width, height: height}}/>

    switch (action) {
      case 'join':
        if (id === undefined) {
          let message = 'id was not passed for action=join';
          render = <Warning message={message}/>
        }
        else {
          render = <Join {...props} mode='join' id={id} additionalInfo={this.state.data}/>
        }
        break;
      case 'preview':
        if (this.state.data != null){
          render = <Join {...props} mode='preview' data={this.state.data}/>
        }
        break;
      default:
        if (this.state.data != null) {
          render = <Create {...props} data={this.state.data}/>
        }
    }

    return (
      <View style={styles.container}>
        {render}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
