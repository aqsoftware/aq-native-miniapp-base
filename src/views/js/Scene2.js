// @flow
import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet
} from 'react-native';
import {
  AQButton
} from '../../components';

/* Define constants here

ex.
const MY_CONSTANT = 42;
*/

export class Scene2 extends Component {
  render(){
    return(
      <View style={styles.container}>
        {/* TODO: insert additional components here as required be the specs document */}
        <View style={styles.createButton}>
          <AQButton
            title='Done'
            backgroundColor='gray'
            width={100}
            height={40}
            onPress={this.props.onActionButtonClick}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
  },
  createButton: {
    position: 'absolute',
    bottom: 100
  }
});
