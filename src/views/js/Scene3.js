// @flow
import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet
} from 'react-native';
import {
  AQButton
} from '../../components';

export class Scene3 extends Component {
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.createButton}>
          <AQButton
            title='Restart'
            backgroundColor='gray'
            width={100}
            height={40}
            onPress={this.props.onActionButtonClick}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
  },
  createButton: {
    position: 'absolute',
    bottom: 100
  }
});
