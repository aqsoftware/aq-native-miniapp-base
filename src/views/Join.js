// @flow
import React, { Component } from 'react';
import { CloudStorage, defaultLifeCycle } from 'aq-native-miniapp';
import {
  View,
  Dimensions,
  StyleSheet
} from 'react-native';
import {
  StaticCanvas,
  Background
} from '../components';
import {
  Scene1,
  Scene2,
  Scene3,
} from './js';

type Props = {
  cloudStorageClient: CloudStorage,
  id?: string,
  data? : Object,
  mode: 'preview' | 'join'
}

export default class Join extends Component {
  state: {
    currentPage: number,
    data: ?Object,
    mode: 'preview' | 'join',
  }

  constructor(props: Props){
    super(props);
    this.state = {
      currentPage: 1,
      data: props.data,
      mode: props.mode
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    if(!nextProps.id) {
      this.setState({data: nextProps.data, currentPage: 1});
    }
  }

  _onJoin1ActionButtonClick(){
    this.setState({currentPage: 2});
  }

  _onJoin2ActionButtonClick(){
    this.setState({currentPage: 3});
  }

  _onJoin3ActionButtonClick(){
    this.setState({currentPage: 1});
    defaultLifeCycle.end();
  }

  render(){
    const data = this.state.data;
    const { height, width } = Dimensions.get('window');
    let render = <StaticCanvas style={{width:width, height: height}}/>

    // if (data) {
      // let source = data.source;
      // if (this.props.additionalInfo && this.props.additionalInfo.passSource) {
      //   source = this.props.additionalInfo.passSource;
      // }
      switch (this.state.currentPage) {
        case 1:
          render =
            <Scene1 style={styles.container}
              onActionButtonClick={this._onJoin1ActionButtonClick.bind(this)}
            />
          break;
        case 2:
          render =
            <Scene2 style={styles.container}
              onActionButtonClick={this._onJoin2ActionButtonClick.bind(this)}
            />
          break;
        case 3:
          render =
            <Scene3 style={styles.container}
              onActionButtonClick={this._onJoin3ActionButtonClick.bind(this)}
            />
          break;
        default:
      }
    // }
    return (
      <View style={styles.container}>
        <Background style={{width:width, height: height}} source={require('../assets/background.jpg')}/>
        {render}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center'
  },
  createButton: {
    position: 'absolute',
    bottom: 100
  }
});
